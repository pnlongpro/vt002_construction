module.exports = function (grunt) {

	//var project = grunt.option('project') || 'agapi';
	//var site = grunt.option('site') || 0;
	//
	//// Added support to WordPress Multisite
	//if (site) {
	//	lessdest = "sources/" + project + "/style-" + site + ".css";
	//} else {
	//	lessdest = "sources/" + project + "/style.css";
	//}
	//
	//var htdocs = grunt.option('htdocs') || 'E:/xampp/htdocs/osthemes/wordpress_cortana';
	//var rootpath = process.cwd();
	//grunt.log.write(rootpath);

	// Project configuration
	var path = 'wp-content/themes/wordpress_cortana';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		//secret: grunt.file.readJSON('secret.json'),
		less: {
			dev: {
				options: {
					compress: true,
					cleancss: true,
					cleancssOptions: {
						'keepSpecialComments': 0
					}
				},
				src: [
					path + 'assets/css/less/style.less'
				],
				dest: 'style.css'
			}
		},
		watch: {
			dev: {
				files: [ path + 'assets/css/less/*.less', path + 'assets/css/less/loading/*.less', path + 'assets/css/less/shortcode/*.less', path + 'assets/css/less/variable.less', path + 'theme-info.txt'],
				tasks: 'default',
				options: {
					livereload: true
				}
			}
		}
	});

	grunt.event.on('watch', function (action, filepath, target) {
		grunt.log.writeln(target + ': ' + filepath + ' has ' + action)
	});

	// Load the plugin that provides the "uglify", "less", "watch" tasks
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s)
	grunt.registerTask('default', ['less:dev','concat:themeinfo']);
};
